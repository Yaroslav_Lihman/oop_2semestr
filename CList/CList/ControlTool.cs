﻿using CList;
using List;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CList
{
    enum Operations
    {
        Add = 1,
        Remove,
        Info,
        Sort,
        Quit,
        SerizalizeBinnary,
        DeserializeBinnary,
        XmlSerialize,
        XmlDeserialize
    }
    [Serializable]
   public class Controller
    {
        public ist<int> Listok = new ist<int>();
        public ist<int> this[int index]
        {
            get { return Listok.IndexOf(index); }
        }
        public Controller()
        {
            Listok.BeforeAdd += IsValidAdd;
            Listok.AfterAdd += MessageHandler ;
            Listok.BeforeRemove += MessageHandler;
            Listok.AfterRemove += MessageHandler;
        }
        public void IsValidAdd(object sender, ListArgs<int> e)
        {
            e.IsValidation = e.Element % 2 == 0;
        }
        public void IsValidRemove(object sender, ListArgs<int> e)
        {
            if(Listok.Count!=0)
            {
                e.IsValidation = true;
            }
            else
            {
                e.IsValidation = false;
                Console.WriteLine("List is clear");
            }
        }
        public void Start()
        {

            while (true)
            {
                Console.WriteLine("Operation:\n1.Add\t\t6.Serialize Binary\n2.Remove\t7.Deserialize Binary\n3.Info\t\t8.Xml Serealization\n4.Sort\t\t9.Xml Deserealization\n5.Quit");
                Operations op = 0;
                Console.Write("Choose: ");
                try
                {
                    op = (Operations)int.Parse(Console.ReadLine());
                    switch (op)
                    {
                        case Operations.Add:
                            Add();
                            break;
                        case Operations.Remove:
                            Remove();
                            break;
                        case Operations.Info:
                            Info();
                            break;
                        case Operations.Sort:
                            SerializeBinary();
                            break;
                        case Operations.Quit:
                            Environment.Exit(1);
                            break;
                        case Operations.SerizalizeBinnary:
                            SerializeBinary();
                            break;
                        case Operations.DeserializeBinnary:
                            DeserializeBinary();
                            break;
                        case Operations.XmlSerialize:
                            XmlSerialization();
                            break;
                        case Operations.XmlDeserialize:
                            XmlDeserialization();
                            break;
                        default:
                            { Console.WriteLine("Invalid operation"); }

                            break;

                    }
                }
                catch (FormatException) { Console.WriteLine("Invalid operation"); }
                catch (InvalidOperationException) { Console.WriteLine("This operation is no exist in list"); }
            }

        }
        public void Add()
        {
            Console.Write("Input your element: ");
            int element = int.Parse(Console.ReadLine());
            if (element > 0 && element < 1000)
            {
                Listok.Add(element);
            }
            else
            {
                Console.WriteLine("Add number in borders of 1000");
            }
        }
        public void Remove()
        {
            Console.Write("Input your index: ");
            int element = int.Parse(Console.ReadLine());
            try
            {
                if (Listok.Count != 0)
                {
                    Listok.RemoveAt(element);
                }
            else
                Console.WriteLine("List is clear, input some elements");
            }
            catch (ArgumentOutOfRangeException) { Console.WriteLine("Inputed index is out of range"); }
        }
        public void Info()
        {
            Console.Write("Sorted List:  ( ");
            foreach (int i in Listok)
            {
                Console.Write($"{i} ");
            }
            Console.Write(" )");
        }
        static void MessageHandler(object sender, ListArgs<int> e)
        {
            Console.WriteLine(e.Message);
            
        }

        public void SerializeBinary()
        {
           
            
        }
        public void DeserializeBinary()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            using (FileStream fs = new FileStream("Listok.dat", FileMode.OpenOrCreate))
            {
                ist<int> NewListok = (ist<int>)formatter.Deserialize(fs);
                Console.WriteLine("Object Deserialized");
                Console.WriteLine("Index: {0}", NewListok.IndexOf(2));
                fs.Close();
            }
             Console.ReadLine();
        }
        public void XmlSerialization()
        {
            XmlSerializer formatter = new XmlSerializer(typeof(ist<int>));
            using (FileStream fs = new FileStream("Numbers.xml", FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, Listok);

                Console.WriteLine("Xml Serialized");
            }
        }
        public void XmlDeserialization()
        {
            XmlSerializer formatter = new XmlSerializer(typeof(ist<int>));
            using (FileStream fs = new FileStream("Numbers.xml", FileMode.OpenOrCreate))
            {
                ist<int> MyNewList = (ist<int>)formatter.Deserialize(fs);
                Console.WriteLine("Object Deserialized");
            }
        }
    }
}

