﻿using System;


namespace CollectionApp
{
    class Program
    {
        static void Main(string[] args)
        {
            
                var tester = new CollectionManager<char>();
                var chars = new char[] { 'solar', 'german', 'credit','book', 'made', 'traveler', 'kisser', 'boom' };
            tester.Generate(chars);
            tester.Sort(SortBy => SortBy> 'o');
            tester.Show();

        }
    }
}
