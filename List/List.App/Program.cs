﻿using List.BL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace List.App
{
    class Program
    {
        static void Main(string[] args)
        {
            var manager = new Manager();
            manager.Start();
        }
    }
}