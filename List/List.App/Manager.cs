﻿using List.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace List.App
{
    enum Action
    {
        Add = 1,
        Remove,
        Sort,
        Quit
    }

    public class Manager
    {
        private CList<int> _myList;

        public Manager()
        {
            _myList = new CList<int>();
            _myList.BeforeAdded += AddRuleHandler;
            _myList.BeforeAdded += MessageHandler;
            _myList.AfterAdded += MessageHandler;
            _myList.BeforeDeleted += MessageHandler;
            _myList.AfterDeleted += MessageHandler;
            _myList.Sorted += MessageHandler;
        }

        private void AddRuleHandler(object sender, ListEventArgs<int> e)
        {
            e.IsValid = e.Value % 2 == 0;
        }

        public void Start()
        {
            while (true)
            {
            
                Console.WriteLine("Меню листа: \n1. Добавить \t3. Отсортировать\n2. Удалить \t4. Выйти \n");
                Action action = 0;
                try
                {
                    action = (Action)int.Parse(Console.ReadLine());
                    switch (action)
                    {
                        case Action.Add:
                            AddItem();
                            break;
                        case Action.Remove:
                            RemoveItem();
                            break;
                        case Action.Sort:
                            _myList.Sort();
                            break;
                        case Action.Quit:
                            Environment.Exit(0);
                            break;
                        default:
                            Console.WriteLine("Некорректная операция\n");
                            break;
                    }
                }
                catch (FormatException) { Console.WriteLine("Некорректная операция.\n"); }
                catch (ArgumentOutOfRangeException) { Console.WriteLine("Некорректный индекс.\n"); }
                catch (OverflowException) { Console.WriteLine($"Некоректное значение"); }
                catch (InvalidOperationException) { Console.WriteLine("Значение не было найдено"); }
            }
        }

        public void AddItem()
        {
            Console.Write("Введите зачение ");
            int value = int.Parse(Console.ReadLine());
                _myList.Add(value);
        }

        public void RemoveItem()
        {
            Console.Write("Введите значение: ");
            int value = int.Parse(Console.ReadLine());
            if (!_myList.Remove(value))
                throw new InvalidOperationException();
        }

        static void MessageHandler(object sender, ListEventArgs<int> e)
        {
            Console.WriteLine(e.Message);
            Console.ReadKey();
        }
    }
}
