﻿using CollectionClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Class
{
    class Program
    {
        static void Main(string[] args)
        {
            Library lib = new Library("Witcher");
            Console.WriteLine(lib[0].BookName);
            lib[1] = new Books("Max Frey");
            Console.WriteLine(lib[1]);
        }
    }
}
