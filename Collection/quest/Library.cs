﻿using Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionClass
{
    class Library
    {
        Books[] Book;
        public Library(string name)
        {
            Book = new Books[] { new Books(name) };
        }
        public Books this[int index]
        {
            get { return Book[index]; }
            set { Book[index] = value; }
        }
    }
}
