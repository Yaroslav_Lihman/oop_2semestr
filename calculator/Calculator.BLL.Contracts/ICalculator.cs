﻿namespace Calculator.BL.Contracts
{
    public interface ICalculator
    {
        double Calculate(string op, double x, double y);
    }
}
