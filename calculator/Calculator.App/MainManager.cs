﻿using Calculator.BL;
using Calculator.BL.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.App
{
    class Manager
    {
        private readonly DCalculator _calculator = new DCalculator();
        private readonly InputtManager _inputManager = new InputtManager();
        private double _varX;
        private double _varY;
        private string _op;

        private void InputData()
        {
            _varX = _inputManager.InputVar("Введите первое число: ");
            _op = _inputManager.InputOperator("Какую операцию выполнить?: ");
            _varY = _inputManager.InputVar("Ведите второе число: ");
        }

        public void Calculate()
        {
            bool check = true;
            while (check)
            {
                InputData();
                try
                {
                    var result = _calculator.Calculate(_op, _varX, _varY);
                    Console.WriteLine($"\n{_varX} {_op} {_varY} = {result}");
                }
                catch (DivideByZeroException)
                {
                    Console.WriteLine("Деление на ноль запрещено!");
                }
                check = false;

            }
        }
    }
}
