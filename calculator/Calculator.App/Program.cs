﻿using System;

namespace Calculator.App
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Delegate_Calculator");
            var manager = new Manager();
            manager.Calculate();
            Console.ReadKey();
        }
    }
}
