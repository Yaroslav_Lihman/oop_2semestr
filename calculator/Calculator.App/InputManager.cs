﻿using Calculator.BL;
using System;

namespace Calculator.App
{
    class InputtManager
    {
        private readonly DCalculator _calculator = new DCalculator();
        public double InputVar(string promptText)
        {
            Console.Write(promptText);
            double tempInput;
            while (!Double.TryParse(Console.ReadLine(), out tempInput))
            {
                Console.WriteLine("Разрешены толко числа.");
                Console.Write(promptText);
            }
            return tempInput;
        }

        public string InputOperator(string Text)
        {
            Console.Write(Text);
            string tempInput = Console.ReadLine();

            while (!_calculator.Operations.ContainsKey(tempInput))
            {
                Console.WriteLine("Введите корректную опперацию: (+, -, *, /)");
                Console.Write(Text);
                tempInput = Console.ReadLine();
            }
            return tempInput;
        }

    }
}
